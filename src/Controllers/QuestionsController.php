<?php

declare(strict_types=1);

namespace Ucc\Controllers;

use Ucc\Services\BeginGameService;
use Ucc\Services\HandleAnswerService;
use Ucc\Services\QuestionService;
use Ucc\Session;

class QuestionsController extends Controller
{
    private QuestionService $questionService;
    private BeginGameService $beginGame;
    private HandleAnswerService $handleAnswerService;

    public function __construct(
        QuestionService $questionService,
        BeginGameService $beginGame,
        HandleAnswerService $handleAnswerService
    ) {
        parent::__construct();
        $this->questionService = $questionService;
        $this->beginGame = $beginGame;
        $this->handleAnswerService = $handleAnswerService;
    }

    public function beginGame(): bool
    {
        $name = $this->requestBody->name ?? null;
        if (empty($name)) {
            return $this->json('You must provide a name', 400);
        }

        $question = $this->beginGame->execute($name);

        return $this->json(['question' => $question], 201);
    }

    public function answerQuestion(int $id): bool
    {
        if (null === Session::get('name')) {
            return $this->json('You must first begin a game', 400);
        }

        if ((int) Session::get('questionCount') > 4) {
            $name = Session::get('name');
            $points = Session::get('points');
            Session::destroy();

            return $this->json(['message' => "Thank you for playing {$name}. Your total score was: {$points} points!"]);
        }

        $answer = $this->requestBody->answer ?? null;
        if (empty($answer)) {
            return $this->json('You must provide an answer', 400);
        }

        try {
            $message = $this->handleAnswerService->execute($id, $answer);
            $question = $this->questionService->getRandomQuestion();

            return $this->json(['message' => $message, 'question' => $question]);
        } catch (\DomainException $e) {
            return $this->json($e->getMessage(), 400);
        } catch (\Exception $e) {
            return $this->json('Something went wrong!', 500);
        }
    }
}
