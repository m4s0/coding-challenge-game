<?php

declare(strict_types=1);

namespace Ucc\Services;

use Ucc\Session;

class HandleAnswerService
{
    private QuestionService $questionService;

    public function __construct(QuestionService $questionService)
    {
        $this->questionService = $questionService;
    }

    public function execute(int $id, string $answer): string
    {
        $questionsAnswered = json_decode(Session::get('questionsAnswered'), true);
        if (in_array($id, $questionsAnswered)) {
            throw new \DomainException('You have already answered that question');
        }

        $points = $this->questionService->getPointsForAnswer($id, $answer);

        $count = (int) Session::get('questionCount') + 1;
        $currentPoints = (int) Session::get('points') + $points;
        Session::set('questionCount', (string) $count);
        Session::set('points', (string) $currentPoints);

        $questionsAnswered[] = $id;
        Session::set('questionsAnswered', json_encode($questionsAnswered));

        return sprintf('Correct answer, your score is %d', $currentPoints);
    }
}
