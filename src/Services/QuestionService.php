<?php

declare(strict_types=1);

namespace Ucc\Services;

use JsonMapper;
use KHerGe\JSON\JSON;
use Ucc\Models\Question;

class QuestionService
{
    const QUESTIONS_PATH = __DIR__.'/../../questions.json';

    private JSON $json;
    private JsonMapper $jsonMapper;
    private array $questions = [];

    public function __construct(JSON $json, JsonMapper $jsonMapper)
    {
        $decoded = $json->decode(file_get_contents(self::QUESTIONS_PATH));
        foreach ($decoded as $index => $jsonDecoded) {
            $this->questions[$index + 1] = $jsonMapper->map($jsonDecoded, new Question());
        }
    }

    public function getRandomQuestion(): array
    {
        $i = random_int(0, count($this->questions));

        return $this->questions[$i - 1]->jsonSerialize();
    }

    public function getPointsForAnswer(int $id, string $answer): int
    {
        $question = $this->getQuestion($id);

        if ($question->getCorrectAnswer() === $answer) {
            return $question->getPoints();
        }

        throw new \DomainException(sprintf('Wrong response for question %d', $id));
    }

    public function getQuestion(int $id): Question
    {
        if (array_key_exists($id, $this->questions)) {
            return $this->questions[$id];
        }

        throw new \DomainException(sprintf('Question with id %s does not exists', $id));
    }
}
