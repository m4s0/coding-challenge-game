<?php

declare(strict_types=1);

namespace Ucc\Services;

use Ucc\Session;

class BeginGameService
{
    private QuestionService $questionService;

    public function __construct(QuestionService $questionService)
    {
        $this->questionService = $questionService;
    }

    public function execute($name): array
    {
        Session::set('name', $name);
        Session::set('questionCount', '1');
        Session::set('points', '0');
        Session::set('questionsAnswered', '{}');

        return $this->questionService->getRandomQuestion();
    }
}
